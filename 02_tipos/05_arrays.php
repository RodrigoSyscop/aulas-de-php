<?php

/**
 * Um array no PHP é um tipo de mapeamento ordenado
 * que associa "valores" a "chaves".
 *
 * O tipo array pode ser utilizado para implementar várias estruturas
 * de dados como listas (vetores), tabelas hash, dicionário, coleções,
 * pilhas, filas e mais.
 */

$recursos = array(
    "banco_a" => 'dba.cluster.aws.amazon.com',
    "log_dir" => '/var/log/wide',
    "redis_cache" => 'redis01.elasticache.aws.amazon.com'
);

print_r($recursos);
var_dump($recursos);

// a partir do PHP 5.4
$recursos = [
    "banco_a" => 'dba.cluster.aws.amazon.com',
    "log_dir" => '/var/log/wide',
    "redis_cache" => 'redis01.elasticache.aws.amazon.com'
];

#print_r($recursos);

/**
 * chaves podem ser "integers" ou "strings"
 * qualquer outro tipo será convertido (cast)
 *
 * Você pode usar inteiros e strings como chaves
 * no mesmo array.
 *
 * Se você não especificar uma chave,
 * será utilizado o próximo inteiro disponível.
 */

$mix = [
    'a' => 'primeiro',
    2 => 'segundo',
    'terceiro',
    'quarto',
    9 => 'quinto',
    'sexto' => 'sexto',
    'último'
];

#var_dump($mix);


/**
 * Acessando elementos de um array
 */

 $a = $mix[2];
 $b = $mix['a'];


 /**
  * Criando um array utilizando a sintaxe de colchetes.
  */

$pessoa['nome'] = 'Rodrigo Vieira';
$pessoa['idade'] = 34;
$pessoa['email'] = 'rodrigo@widesoft.net';
$pessoa[] = 'próximo inteiro disponível';

#print_r($pessoa);


/**
 * arrays multi-dimensionais
 */

 $pessoa['endereco']['rua'] = 'Av. Brasil';
 $pessoa['endereco']['num'] = '123';
 $pessoa['endereco']['cep'] = '79.091-080';
 $pessoa['endereco']['cidade'] = 'Florianópolis';
 $pessoa['endereco']['uf'] = 'SC';

#print_r($pessoa);

/**
 * ou
 */

$pessoa = [
    'nome' => 'Rodrigo Vieira',
    'idade' => 34,
    'email' => 'rodrigo@widesoft.net',
    '0' => 'próximo inteiro disponível',
    'endereco' => [
            'rua' => 'Av. Brasil',
            'num' => 123,
            'cep' => '79.091-080',
            'cidade' => 'Florianópolis',
            'uf' => 'SC',
    ]
 ];

 #print_r($pessoa);


/**
 * funções úteis
 */

#unset($pessoa[0]);
#print_r(array_keys($pessoa));
#print_r(array_values($pessoa));

/**
 * sempre use aspas quando a chave do array for uma string
 */

// correto
$pessoa['nome'];

// incorreto
$pessoa[nome];
