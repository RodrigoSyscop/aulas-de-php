<?php

/**
 * "Iterable" é um pseudo-tipo introduzido no PHP 7.1.
 * Pode ser um array ou um objeto que impelemente a interface "Transversable".
 *
 * Ambos são iteráveis via foreach e podem ser utilizados num generator via
 * "yield from".
 */
