<?php

/**
 * O tipo "resource" é um tipo especial que armazena uma referência
 * para um recurso externo:
 * - conexão com banco.
 * - arquivo aberto.
 * - conexão ftp.
 * - imagem aberta.
 * - conexão com e-mail.
 * - conexão ldap.
 * ...
 */
