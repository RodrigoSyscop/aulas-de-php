<?php

/**
 * São funções ou métodos que podem ser executados (callable a partir do php 5.4).
 */

function oi() {
    echo "Oi";
}

call_user_func('oi');
