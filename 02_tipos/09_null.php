<?php

/**
 * O tipo NULL representa uma variável sem valor.
 * NULL é o único valor possível para o tipo null.
 *
 * - atribuição direta a constante NULL.
 * - não foi atribuída a nenhum valor ainda.
 * - foi usada na função unset()
 */

$a = NULL;
 
var_dump($a);

#$b = 'rodrigo';
#unset($b);
#var_dump($b);
